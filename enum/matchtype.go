package enum

// MatchStatus enum
type MatchStatus int

// MatchVisibility enum
type MatchVisibility int

// MatchFollowMode enum
type MatchFollowMode int

const (
	created     MatchStatus = 0
	playing     MatchStatus = 1
	interrupted MatchStatus = 2
	stopped     MatchStatus = 3
	finished    MatchStatus = 4
)

const (
	public  MatchVisibility = 0
	club    MatchVisibility = 2
	private MatchVisibility = 3
)

const (
	point MatchFollowMode = 0
	game  MatchFollowMode = 2
)
