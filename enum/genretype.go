package enum

//Genre enum
type Genre int

const (
	men   Genre = 1
	women Genre = 2
)
