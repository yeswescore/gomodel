package community

import (
	"bitbucket.org/yeswescore/gomodel/location"
	"bitbucket.org/yeswescore/gomodel/people"
)

// Club type
type Club struct {
	Code               string          `json:"code"`
	Name               string          `json:"name"`
	League             string          `json:"league"`
	Commitee           string          `json:"commitee"`
	Active             bool            `json:"active"`
	Enterprise         bool            `json:"enterprise"`
	Type               string          `json:"type"`
	Sport              string          `json:"sport"`
	Phone              string          `json:"phone"`
	Fax                string          `json:"fax"`
	Mail               string          `json:"mail"`
	Web                string          `json:"web"`
	Contact            people.Person   `json:"contact"`
	Sector             string          `json:"sector"`
	President          people.Person   `json:"president"`
	IndoorPlaygrounds  int             `json:"indoorPlaygrounds"`
	OutdoorPlaygrounds int             `json:"outdoorPlaygrounds"`
	TennisPlaygrounds  int             `json:"tennisPlaygrounds"`
	PadelPlaygrounds   int             `json:"padelPlaygrounds"`
	BeachPlaygrounds   int             `json:"beachPlaygrounds"`
	Clubhouse          bool            `json:"clubhouse"`
	Omnisport          bool            `json:"omnisport"`
	Geoloc             location.Geoloc `json:"_geoloc"`
}
