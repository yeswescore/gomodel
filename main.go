package main

import (
	"fmt"
	"os"

	"bitbucket.org/yeswescore/gomodel/tennis"
	"github.com/algolia/algoliasearch-client-go/algolia/search"
	"github.com/algolia/algoliasearch-client-go/algolia/opt"
)

func main() {
	client := search.NewClient("F1B63H4OXM", "8cd7c15b59608039753b661a6c806b52")
	index := client.InitIndex("Match")

	_, err := index.SetSettings(search.Settings{
		SearchableAttributes: opt.SearchableAttributes(
			"status",
		),
		CustomRanking: opt.CustomRanking("desc(indexDate)"),
		Ranking: opt.Ranking(
			// "typo",
			"custom",
			"geo",
			"exact",
		),
	})

	res, err := index.Search("created", opt.AroundLatLng("50.40, 3.05"), opt.AroundRadius(100000))

	var matchs []tennis.Match

	err = res.UnmarshalHits(&matchs)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	for i := 0; i < len(matchs); i++ {
		fmt.Printf("Lieu %s\t\t\t\t\tDate : %s\nId : %s\n", matchs[i].Place, matchs[i].ExpectedDate, matchs[i].ID)
	}
}
