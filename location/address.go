package location

// Geoloc type
type Geoloc struct {
	Lat float64 `json:"lat"`
	Lng float64 `json:"lng"`
}

// Address type
type Address struct {
	Firstline  string `json:"firstLine"`
	Secondline string `json:"secondLine"`
	Zip        string `json:"zip"`
	City       string `json:"city"`
}