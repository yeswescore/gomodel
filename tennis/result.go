package tennis

// Scores : list of score
type Scores []int

// Result type
type Result struct {
	SetOne      int    `json:"setOne"`
	SetTwo      int    `json:"setTwo"`
	SetCount    int    `json:"setCount"`
	SetScoreOne Scores `json:"setScoreOne"`
	SetScoreTwo Scores `json:"setScoreTwo"`
	GameOne     int    `json:"gameOne"`
	GameTwo     int    `json:"gameTwo"`
	Server      string `json:"server"`
	Winner      string `json:"winner"`
}
