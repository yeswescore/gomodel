package tennis

import (
	"bitbucket.org/yeswescore/gomodel/location"
	"bitbucket.org/yeswescore/gomodel/people"
)

// Points : List of layer type
type Points []int

// Match type
type Match struct {
	ID			  string 			`json:"objectID"`
	ExpectedDate  string            `json:"expectedDate"`
	StartDate     string            `json:"startDate"`
	EndDate       string            `json:"endDate"`
	IndexDate     int               `json:"indexDate"`
	Place         string            `json:"place"`
	Geoloc        location.Geoloc   `json:"_geoloc"`
	TeamOne       people.Team       `json:"teamOne"`
	TeamTwo       people.Team       `json:"teamTwo"`
	Status        string            `json:"status"`
	Owner         string            `json:"owner"`
	Referee       string            `json:"referee"`
	RulesID       int               `json:"rulesID"`
	Tr            Rule              `json:"tr"`
	PointVector   Points            `json:"pointVector"`
	Mr            Result            `json:"mr"`
	IsDouble      bool              `json:"isDouble"`
	Server        string            `json:"server"`
	InitialServer string            `json:"initialServer"`
	FollowMode    string            `json:"followMode"`
	MoreData      map[string]string `json:"moreData"`
}
