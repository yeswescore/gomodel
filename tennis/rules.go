package tennis

// Rule type
type Rule struct {
	Winningsets                   int  `json:"winningsets"`
	WinningGames                  int  `json:"winningGames"`
	Advantage                     bool `json:"advantage"`
	BlankGameWinningPoint         int  `json:"blankGameWinningPoint"`
	BlankTBWinningPoint           int  `json:"blankTBWinningPoint"`
	BlankSTBWinningPoint          int  `json:"blankSTBWinningPoint"`
	Tiebreak                      bool `json:"tiebreak"`
	StartTiebreakScore            int  `json:"startTiebreakScore"`
	PointsInTiebreak              int  `json:"pointsInTiebreak"`
	SuperTiebreak                 bool `json:"superTiebreak"`
	PointsInSuperTiebreak         int  `json:"pointsInSuperTiebreak"`
	NumberOfGameInLastSet         int  `json:"numberOfGameInLastSet"`
	TiebreakInLastSet             bool `json:"tiebreakInLastSet"`
	StartScoreInTiebreakInLastSet int  `json:"startScoreInTiebreakInLastSet"`
	NumberOfPointsInLastTiebreak  int  `json:"numberOfPointsInLastTiebreak"`
}
