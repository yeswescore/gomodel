package people

// Team type
type Team struct {
	Name    string  `json:"name"`
	Players Players `json:"players"`
	Captain Player  `json:"captain"`
	Coach   Person  `json:"coach"`
}
