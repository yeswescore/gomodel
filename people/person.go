package people

import "bitbucket.org/yeswescore/gomodel/location"

// Person type
type Person struct {
	Civility  string           `json:"civility"`
	FirstName string           `json:"firstName"`
	LastName  string           `json:"lastName"`
	Alias     string           `json:"alias"`
	Email     string           `json:"email"`
	Phone     string           `json:"phone"`
	Address   location.Address `json:"address"`
	Picture   string           `json:"pictureUrl"`
}
