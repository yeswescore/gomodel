package people

import (
	"bitbucket.org/yeswescore/gomodel/location"
)

// Players : List of layer type
type Players []Player

// Player type
type Player struct {
	ID           string           `json:"id"`
	CreationDate int64            `json:"creationDate"`
	Civility     string           `json:"civility"`
	FirstName    string           `json:"firstName"`
	LastName     string           `json:"lastName"`
	Alias        string           `json:"alias"`
	Email        string           `json:"email"`
	Phone        string           `json:"phone"`
	Address      location.Address `json:"address"`
	Picture      string           `json:"pictureUrl"`
	IsPrivate    bool             `json:"isPrivate"`
	Dob          string           `json:"dob"`
	Genre        string           `json:"genre"`
	Rank         string           `json:"rank"`
	Club         string           `json:"club"`
	Licence      string           `json:"licence"`
}
